package org.yamevetwo.gralidation

import groovy.transform.Immutable

@Immutable
class GralidationError {
    String propertyName
    String errorCode
    String value
    String expected
}
