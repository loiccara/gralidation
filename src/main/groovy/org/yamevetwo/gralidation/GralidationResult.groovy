package org.yamevetwo.gralidation

import groovy.transform.Immutable

@Immutable
class GralidationResult {
    boolean isValid
    List<GralidationError> errors
}
