package org.yamevetwo.gralidation

class GralidationException extends RuntimeException {
    GralidationException(String message){
        super(message)
    }

    GralidationException(String message, Throwable cause){
        super(message, cause)
    }
}
