package org.yamevetwo.gralidation

import org.apache.commons.validator.routines.EmailValidator
import org.apache.commons.validator.routines.UrlValidator

import java.util.regex.Pattern

enum GralidationEnum {

    BLANK("blank", false, {String propertyName, def parameterToControl, boolean isBlankable ->
        boolean result = isBlankable || parameterToControl==null || (!isBlankable && parameterToControl?.trim())
        new ControlResult(isValid:result, error:result?null:getError("blank", propertyName, parameterToControl, isBlankable))
    }),
    EMAIL("email", false, {String propertyName, def parameterToControl, boolean isEmailExpected ->
        boolean isEmail = EmailValidator.getInstance(true).isValid(parameterToControl)
        boolean result = (isEmailExpected && isEmail) || (!isEmailExpected && !isEmail)
        new ControlResult(isValid:result, error:result?null:getError("email", propertyName, parameterToControl, isEmailExpected))
    }),
    INLIST("inlist", false, {String propertyName, def parameterToControl, List allowedValues ->
        boolean result = parameterToControl in allowedValues
        new ControlResult(isValid:result, error:result?null:getError("inlist", propertyName, parameterToControl, allowedValues))
    }),
    MATCHES("matches", false, { String propertyName, def parameterToControl, Pattern pattern->
        boolean result = (parameterToControl ==~ pattern)
        new ControlResult(isValid:result, error:result?null:getError("matches", propertyName, parameterToControl, pattern))
    }),
    MAX("max", false, {String propertyName, def parameterToControl, def max ->
        boolean result = parameterToControl <= max
        new ControlResult(isValid:result, error:result?null:getError("max", propertyName, parameterToControl, max))
    }),
    MAXSIZE("maxsize", false, {String propertyName, def parameterToControl, int maxsize ->
        boolean result = parameterToControl?.size()<=maxsize
        new ControlResult(isValid:result, error:result?null:getError("maxsize", propertyName, parameterToControl, maxsize))
    }),
    MIN("min", false, {String propertyName, def parameterToControl, def min ->
        boolean result = parameterToControl > min
        new ControlResult(isValid:result, error:result?null:getError("min", propertyName, parameterToControl, min))
    }),
    MINSIZE("minsize", false, {String propertyName, def parameterToControl, int minsize ->
        boolean result = parameterToControl?.size()>=minsize
        new ControlResult(isValid:result, error:result?null:getError("minsize", propertyName, parameterToControl, minsize))
    }),
    NOTEQUAL("notequal", false, {String propertyName, def parameterToControl, def value ->
        boolean result = parameterToControl!=value
        new ControlResult(isValid:result, error:result?null:getError("notequal", propertyName, parameterToControl, value))
    }),
    NULLABLE("nullable", false, {String propertyName, def parameterToControl, boolean isNullable ->
        boolean result = isNullable || (!isNullable && parameterToControl!=null)
        new ControlResult(isValid:result, error:result?null:getError("nullable", propertyName, parameterToControl, isNullable))
    }),
    RANGE("range", false, {String propertyName, def parameterToControl, def range ->
        boolean result = parameterToControl in range
        new ControlResult(isValid: result, error: result?null:getError("range", propertyName, parameterToControl, range))
    }),
    TYPE("type", false, {String propertyName, def parameterToControl, TypeCheck typeCheck ->
        boolean result = typeCheck.check.call(parameterToControl)
        new ControlResult(isValid:result, error:result?null:getError("type", propertyName, parameterToControl, typeCheck))
    }),
    URL("url", false, {String propertyName, def parameterToControl, def urlExpected ->
        boolean isUrl = UrlValidator.getInstance().isValid(parameterToControl)
        boolean result = (urlExpected && isUrl) || (!urlExpected && !isUrl)
        new ControlResult(isValid:result, error:result?null:getError("email", propertyName, parameterToControl, urlExpected))
    }),
    EACH("each", true, {String propertyName, List parameterToControl, Map controls ->
        Gralidator.controlList(propertyName, parameterToControl, controls)
    }),
    EACHKEY("eachkey", true, {String propertyName, Map parameterToControl, Map controls ->
        Gralidator.controlList(propertyName, parameterToControl?.keySet()?.toList(), controls)
    }),
    CUSTOM("custom", false, {String propertyName, def parameterToControl, Closure customControl ->
        def controlResult = customControl.call()
        if (controlResult.class != ControlResult.class){
            throw new GralidationException("The result of the closure should be a ControlResult (check gralidation documentation")
        }
        controlResult
    })

    final String value
    final boolean isMultipleControl
    final Closure control

    GralidationEnum(String value, isMultipleControl, Closure control){
        this.value = value
        this.isMultipleControl = isMultipleControl
        this.control = control
    }

    private static GralidationError getError(String suffixeErrorCode, String propertyName, def value, def expected){
        String errorCode = Gralidator.ERROR_CODE_PREFIX + suffixeErrorCode
        new GralidationError(propertyName:propertyName, errorCode: errorCode, value:value, expected:expected)
    }
}