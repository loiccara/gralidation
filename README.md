# What is this repository for? #

Gralidation helps you not to waste time while validating data. Inspired by Gorm, powered by Groovy, developed for developers who do not want to do manual validation. 

With this library, you can validate Objects and Maps, by creating a map of constraints.

# How to use it? #

***How to validate an object:***
```
#!groovy
Class SuperHero {
   String name
   List powers
   String weakness

   Map constraints = [
     name:[nullable:false, blank:false]
   ]
}

```

You can get the validation result by:

```
#!groovy

SuperHero batman = new SuperHero(name:"Batman") // Batman has no power and no weakness
GralidationResult gralidationResult = Gralidator.gralidate(batman)
```
***How to validate a Map:***

```
#!groovy

Map batman = [name:batman, powers:[], weakness:""]
Map superman = [name:superman, powers:["Frozen breath", "Lasers", "Fly", "Amazing strength"], weakness:"kryptonite"]

Map constraints = [
  name:[nullable:false, blank:false],
]

```

You can get the validation result by:

```
#!groovy

GralidationResult gralidationResult = Gralidator.gralidate(batman)
```

# How to use a GralidationResult? #

The object itself:
```
#!groovy

@Immutable
class GralidationResult {
    boolean isValid
    List<GralidationError> errors
}
```

How to check if this result is valid:
```
#!groovy
GralidationResult gralidationResult = Gralidator.gralidate(anObjectOrAMap)
gralidationResult.isValid
``` 


How to handle the errors:

A list of GralidationError objects is provided for each validation through the Gralidation library. 

```
#!groovy
@Immutable
class GralidationError {
    String propertyName
    String errorCode
    String value
    String expected
}
``` 

# How do I get set up? ##

### Installation ###
1. Option 1: add the gradle dependency (latest version)
http://mvnrepository.com/artifact/org.yamevetwo/gralidation/1.2

2. Option 2: download the jar in your project
This solution is not recommended: only fully tested releases should be used.

### Configuration ###
There are dependencies on Groovy 2.4 and Spock. 

To ensure that your validation is read properly, you need to create a map of constraints. This map needs to be in your Object class in case of an object validation. E.g.:

Map constraints = [
  name:[nullable:true, blank:false],
  age:[nullable:false]
]

The result is a new object, containing:
- a boolean isValid, true if the validation is correct
- a list of errors, containing all the validation errors in case of a failure during the validation.

### Available validation controls ###
| Control name | Compatible with all JVM languages	| description: test if								| example to include in your map of constraints |
|--------------|------------------------------------|---------------------------------------------------|-----------------------------------------------|
| blank        | yes							   	| a String is empty ("" or only spaces) 			| name:[blank:true] 							|
| email        | yes							   	| a String matches an email pattern					| myEmail:[email:true] 							|
| inlist       | yes							   	| a value is in this list of values					| things:[nullable:[1,5,10]] 					|
| matches      | yes							   	| matches the given regex pattern 					| myString:[matches:~/.{4}/] 					|
| max          | yes							   	| a value is lower than the given maximum			| wives:[max:1] 								|
| maxsize      | yes							   	| a collection is smaller than <maxsize> elements	| superPowers:[maxsize:5] 						|
| min          | yes							   	| a value is higher than the given minimum			| age:[min:3] 									|
| minsize      | yes							   	| a collection is bigger than <minsize> elements	| importantThing[minsize:16]					|
| notequal     | yes							   	| a value is not equal to the given values			| president:[notequal:'Donald Trump']			|
| nullable     | yes							   	| a value is nullable								| name:[nullable:false]							|
| range        | yes							   	| a value is included in the given range			| testicles[range:1..3]							|
| type         | yes							   	| a value has a given type, useful for data Maps	| age:[type:'Integer']							|
| url          | yes							   	| a value matches a URL pattern						| website:[url:true]							|
| each 		   | yes								| all values of a list pass a list of given controls| names:[each:[maxsize:10]]						|
| eachkey	   | yes								| all keys of a map pass a list of given controls 	| openingHours:[eachkey:[inlist:daysOfTheWeek]]	|
| custom	   | **NO** 							| a value passes a custom control 					| (coming soon)									|

### Contribution guidelines ### 

Please feel free to clone this library, expand it, and submit pull request.

### Who do I talk to? ###

Loic Cara
cara.loic.pro@gmail.com